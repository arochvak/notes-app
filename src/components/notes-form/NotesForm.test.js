import React from 'react';
import { mount } from 'enzyme';
import TextField from '@material-ui/core/TextField';
import NotesFormWithStyle, { NotesForm } from './NotesForm';
import NotesContext from '../../state/NotesContext';

describe('NotesForm', () => {
  let wrapper;
  const contextValue = {
    notes: [],
    onAddNewNote: jest.fn(),
    onGoOutOfPreviewMode: jest.fn()
  };

  beforeEach(() => {
    wrapper = mount(
      <NotesContext.Provider value={contextValue}>
        <NotesFormWithStyle />
      </NotesContext.Provider>
    );
  });

  it('should be invalid after render, because we have required fields', () => {
    const previewBtn = wrapper.find('button#previewBtn');
    const submitBtn = wrapper.find('button#submitBtn');
    expect(previewBtn.prop('disabled')).toBeTruthy();
    expect(submitBtn.prop('disabled')).toBeTruthy();
  });

  it('should render invalid text fields in the right way', () => {
    const name = wrapper.find('#name').find(TextField);
    const text = wrapper.find('#text').find(TextField);
    expect(name.prop('error')).toBeTruthy();
    expect(text.prop('error')).toBeTruthy();
  });

  it('should call onAddNewNote with proper args', () => {
    const values = {
      text: 'text',
      name: 'name'
    };
    const form = wrapper.find(NotesForm);
    const formInstance = form.instance();
    form.setState(values);
    formInstance.onSubmit();
    expect(contextValue.onAddNewNote).toBeCalledWith({
      ...values,
      avatar: '',
      backgroundColor: '',
      textColor: ''
    });
  });
});
