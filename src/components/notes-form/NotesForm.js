import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import NotesContext from '../../state/NotesContext';
import TextFieldWithValidation from '../common/TextFieldWithValidation';
import validators from '../../utils/validators';

const styles = {
  root: {
    display: 'flex',
    flexDirection: 'column',
    flexBasis: '16em',
    flexGrow: 1,
    padding: '1em',
    margin: '0 1em 1em 1em'
  },
  textField: {
    margin: '0.5em 0'
  },
  button: {
    margin: '0.5em 0'
  },
  header: {
    textAlign: 'center'
  }
};

const fieldsMap = {
  text: 0,
  name: 1,
  avatar: 2,
  textColor: 3,
  backgroundColor: 4
};

class NotesForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      name: '',
      avatar: '',
      textColor: '',
      backgroundColor: '',
      isInPreviewMode: false
    };
    this.validityFlags = [true];
    this.onNameChange = this.handleChange.bind(this, 'name');
    this.onTextChange = this.handleChange.bind(this, 'text');
    this.onAvatarChange = this.handleChange.bind(this, 'avatar');
    this.onTextColorChange = this.handleChange.bind(this, 'textColor');
    this.onBackgroundColorChange = this.handleChange.bind(
      this,
      'backgroundColor'
    );
    this.onSubmit = this.onSubmit.bind(this);
    this.onPreview = this.onPreview.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  onSubmit(e, isPrev) {
    const { name, text, avatar, textColor, backgroundColor } = this.state;
    const { onAddNewNote } = this.context;
    onAddNewNote({
      name,
      text,
      avatar,
      textColor,
      backgroundColor
    });
    if (!isPrev) {
      this.setState({
        text: '',
        avatar: '',
        textColor: '',
        backgroundColor: ''
      });
    }
  }

  onPreview(e) {
    this.setState({ isInPreviewMode: true });
    this.onSubmit(e, true);
  }

  onCancel() {
    this.setState({ isInPreviewMode: false });
    const { onGoOutOfPreviewMode } = this.context;
    onGoOutOfPreviewMode();
  }

  handleChange(field, value, isValid) {
    this.validityFlags[fieldsMap[field]] = isValid;
    this.setState({
      [field]: value
    });
  }

  render() {
    const { classes } = this.props;
    const {
      name,
      text,
      avatar,
      textColor,
      backgroundColor,
      isInPreviewMode
    } = this.state;
    const isNoteInvalid = !this.validityFlags.every(f => f);
    return (
      <Paper className={classes.root}>
        <h3 className={classes.header}>Add new note:</h3>
        <TextFieldWithValidation
          id="name"
          label="Your name"
          className={classes.textField}
          value={name}
          onChange={this.onNameChange}
          margin="normal"
          validators={[validators.required]}
        />
        <TextFieldWithValidation
          id="text"
          label="Text of your note"
          className={classes.textField}
          value={text}
          onChange={this.onTextChange}
          margin="normal"
          multiline
          rows="4"
          validators={[validators.required]}
        />
        <TextFieldWithValidation
          id="avatar"
          label="Link to your avatar"
          className={classes.textField}
          value={avatar}
          onChange={this.onAvatarChange}
          margin="normal"
          validators={[validators.isImgUrl]}
        />
        <TextFieldWithValidation
          id="textcolor"
          label="Hex color for text"
          className={classes.textField}
          value={textColor}
          onChange={this.onTextColorChange}
          margin="normal"
          validators={[validators.isHexColor]}
        />
        <TextFieldWithValidation
          id="backgroundcolor"
          label="Hex color for background"
          className={classes.textField}
          value={backgroundColor}
          onChange={this.onBackgroundColorChange}
          margin="normal"
          validators={[validators.isHexColor]}
        />
        {isInPreviewMode ? (
          <Button
            id="backBtn"
            variant="contained"
            className={classes.button}
            onClick={this.onCancel}
          >
            Back
          </Button>
        ) : (
          <>
            <Button
              variant="contained"
              id="previewBtn"
              className={classes.button}
              onClick={this.onPreview}
              disabled={isNoteInvalid}
            >
              Preview
            </Button>
            <Button
              variant="contained"
              id="submitBtn"
              className={classes.button}
              onClick={this.onSubmit}
              disabled={isNoteInvalid}
            >
              Submit
            </Button>
          </>
        )}
      </Paper>
    );
  }
}

NotesForm.contextType = NotesContext;

NotesForm.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string.isRequired,
    textField: PropTypes.string.isRequired,
    button: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired
  }).isRequired
};

export { NotesForm };

export default withStyles(styles)(NotesForm);
