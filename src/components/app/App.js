import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
// eslint-disable-next-line import/no-named-as-default
import NotesForm from '../notes-form/NotesForm';
import NotesGrid from '../grid/NotesGrid';

const styles = {
  root: {
    display: 'flex',
    alignItems: 'start',
    flexWrap: 'wrap',
    padding: '1em'
  }
};

const App = ({ classes }) => {
  return (
    <div className={classes.root}>
      <NotesForm />
      <NotesGrid />
    </div>
  );
};

App.propTypes = {
  classes: PropTypes.shape({ root: PropTypes.string.isRequired }).isRequired
};

export default withStyles(styles)(App);
