import React from 'react';
import NotesContext from '../../state/NotesContext';
import App from './App';
import generalUtils from '../../utils/general';

class AppContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      notes: [],
      // eslint-disable-next-line react/no-unused-state
      onAddNewNote: this.onAddNewNote.bind(this),
      // eslint-disable-next-line react/no-unused-state
      onGoOutOfPreviewMode: this.onGoOutOfPreviewMode.bind(this)
    };
  }

  onAddNewNote(note) {
    this.setState(state => ({
      notes: [
        ...state.notes,
        {
          id: generalUtils.getUUIDv4(),
          date: new Date().toLocaleDateString(),
          ...note
        }
      ]
    }));
  }

  onGoOutOfPreviewMode() {
    this.setState(state => ({
      notes: state.notes.slice(0, state.notes.length - 1)
    }));
  }

  render() {
    return (
      <NotesContext.Provider value={this.state}>
        <App />
      </NotesContext.Provider>
    );
  }
}

export default AppContainer;
