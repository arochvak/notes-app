import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import NotesContext from '../../state/NotesContext';
import Note from './Note';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flexGrow: 999,
    flexBasis: 0,
    minWidth: '50%'
  }
};

const NotesGrid = ({ classes }) => {
  return (
    <NotesContext.Consumer>
      {({ notes }) => (
        <div className={classes.root}>
          {notes.map(n => (
            // eslint-disable-next-line react/jsx-props-no-spreading
            <Note key={n.id} {...n} />
          ))}
        </div>
      )}
    </NotesContext.Consumer>
  );
};

NotesGrid.propTypes = {
  classes: PropTypes.shape({ root: PropTypes.string.isRequired }).isRequired
};

export default withStyles(styles)(NotesGrid);
