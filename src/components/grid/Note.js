import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    margin: '0 1em 2em 1em;',
    width: '16em'
  },
  typography: {
    wordBreak: 'break-word'
  }
};

const Note = ({
  classes,
  text,
  name,
  avatar,
  date,
  textColor,
  backgroundColor
}) => {
  return (
    <Card
      className={`${classes.card} note`}
      style={{ background: backgroundColor }}
    >
      <CardHeader
        className="note__header"
        avatar={
          avatar ? (
            <Avatar aria-label="recipe" src={avatar} />
          ) : (
            <Avatar aria-label="recipe">{name[0]}</Avatar>
          )
        }
        title={name}
        subheader={date}
      />
      <CardContent>
        <Typography
          component="div"
          style={{ color: textColor }}
          className={`${classes.typography} note__text`}
        >
          {text.split('\n').map((p, key) => (
            // eslint-disable-next-line react/no-array-index-key
            <p key={key}>{p}</p>
          ))}
        </Typography>
      </CardContent>
    </Card>
  );
};

Note.propTypes = {
  classes: PropTypes.shape({
    card: PropTypes.string.isRequired,
    typography: PropTypes.string.isRequired
  }).isRequired,
  text: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  textColor: PropTypes.string,
  backgroundColor: PropTypes.string
};

Note.defaultProps = {
  avatar: '',
  textColor: '',
  backgroundColor: ''
};

export default withStyles(styles)(Note);
