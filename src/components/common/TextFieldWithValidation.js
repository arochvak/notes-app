import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

class TextFieldWithValidation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isValid: false, value: undefined };
    this.onChange = this.onChange.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (state.value === props.value) {
      return null;
    }

    const isValid = props.validators.every(v => v(props.value));
    props.onChange(props.value, isValid);

    return {
      value: props.value,
      isValid
    };
  }

  componentDidMount() {
    const { isValid } = this.state;
    const { onChange } = this.props;
    if (!isValid) {
      onChange('', false);
    }
  }

  onChange(e) {
    const newValue = e.target.value;
    const { onChange, validators } = this.props;
    const isValid = validators.every(v => v(newValue));
    onChange(newValue, isValid);
    this.setState({ isValid });
  }

  render() {
    const { isValid } = this.state;
    return (
      // eslint-disable-next-line react/jsx-props-no-spreading
      <TextField {...this.props} error={!isValid} onChange={this.onChange} />
    );
  }
}

TextFieldWithValidation.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  validators: PropTypes.array.isRequired
};

export default TextFieldWithValidation;
