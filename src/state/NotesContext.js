import React from 'react';

const NotesContext = React.createContext({
  notes: [],
  onAddNewNote: () => {},
  onGoOutOfPreviewMode: () => {}
});

export default NotesContext;
