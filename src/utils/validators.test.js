import validators from './validators';

describe('validators', () => {
  it('should fail for empty field if it is requred', () => {
    const result = validators.required('');
    expect(result).toBeFalsy();
  });

  it('should be valid for not empty field if it is required', () => {
    const result = validators.required('some');
    expect(result).toBeTruthy();
  });

  it('should fail for wrong url to image', () => {
    const result = validators.isImgUrl('not an url');
    expect(result).toBeFalsy();
  });

  it('should be valid if url is empty (because it is not required)', () => {
    const result = validators.isImgUrl('');
    expect(result).toBeTruthy();
  });

  it('should be valid if url to img is valid', () => {
    const result = validators.isImgUrl('http://domen.com/img.jpg');
    expect(result).toBeTruthy();
  });

  it('should fail for wrong color', () => {
    const result = validators.isHexColor('not a color');
    expect(result).toBeFalsy();
  });

  it('should be valid if color is empty (because it is not required)', () => {
    const result = validators.isHexColor('');
    expect(result).toBeTruthy();
  });

  it('should be valid if color is valid', () => {
    const result = validators.isHexColor('#fff');
    expect(result).toBeTruthy();
  });
});
