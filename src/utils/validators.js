export default {
  required(val) {
    return val.length > 0;
  },
  isHexColor(val) {
    return val.length === 0 || val.match(/^#([0-9a-f]{3}|[0-9a-f]{6})$/i);
  },
  isImgUrl(val) {
    return (
      val.length === 0 ||
      val.match(/(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/i)
    );
  }
};
