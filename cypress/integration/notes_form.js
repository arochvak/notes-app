/* eslint-disable no-undef */
const home = 'http://localhost:3000';
const name = 'my name';
const text = 'text for my note';

describe('notes form', () => {
  it('should render with disable buttons', () => {
    cy.visit(home);
    cy.get('button#submitBtn').should('be.disabled');
    cy.get('button#previewBtn').should('be.disabled');
  });

  it('should not be able to post a note with only text', () => {
    cy.visit(home);
    cy.get('#text')
      .type(text)
      .should('have.value', text);
    cy.get('button#submitBtn').should('be.disabled');
    cy.get('button#previewBtn').should('be.disabled');
  });

  it('should not be able to post a note with only name', () => {
    cy.visit(home);
    cy.get('#name')
      .type(name)
      .should('have.value', name);
    cy.get('button#submitBtn').should('be.disabled');
    cy.get('button#previewBtn').should('be.disabled');
  });

  it('should be able to post a note with text and name', () => {
    cy.visit(home);
    cy.get('#text')
      .type(text)
      .should('have.value', text);
    cy.get('#name')
      .type(name)
      .should('have.value', name);
    cy.get('button#submitBtn').should('be.not.disabled');
    cy.get('button#previewBtn').should('be.not.disabled');
    cy.get('button#submitBtn').click();

    cy.get('.note').should('have.length', 1);
    cy.get('.note__header').contains(name);
    cy.get('.note__text').contains(text);
  });
});
