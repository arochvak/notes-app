# Project description

It's a test project. Task:
Write a simple public sticky notes ( POST-IT notes) board.

Any visitor can post a sticky note on the board. The note must have a text on it, date when it was posted, and should display the name of the author and gravatar image if it's available. User can choose the colour of the note.

Please, try to provide the best possible user experience, users would like to see how their note would look like on the board among others prior to saving it there.

Please, do your best while writing the code, imagine that you are going to deploy it to production as a part of large application, therefore you should apply all the disciplines and principles as for writing production code.

### Note:

For simplicity:

- colours should be set as a hex colour (#fff, etc.)

- you should set your avatar as a link to an image (http://***.jpg, etc.)

# Technical details

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[Material-UI](https://material-ui.com/) was used to speed up development. Currently, there are no wrappers for material-ui components. But if it would be a real project, then they would be required to make a switch from the library faster (in case we decide to use another library or make our own UI kit). The only one thing that can be considered as a wrapper is 'src\components\common\TextFieldWithValidation.js'.

Linter & Prettier are used for code formatting. As a [config AirBnB](https://github.com/airbnb/javascript/blob/master/packages/eslint-config-airbnb/README.md) style config is used. If you are using VS Code, you also can look at [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) and [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) plugins.

Currently, here is installed husky & lint-staged (to run Prettier and ESLint on pre-commit hook) but it doesn't work under my VSCode for some reason. Stoped investigation due to limit in time for this task. Should be investigated later...

To speed up the process, there are only a few tests examples:

- unit tests src\utils\validators.test.js
- unit tests with enzyme src\components\notes-form\NotesForm.test.js
- e2e tests with cypress cypress\integration\notes_form.js

About CI/CD - currently, I continuously see an error with cypress tests on Bitbucket, but it seems to be a new bug on [cypress](https://github.com/cypress-io/cypress/issues/5110#issuecomment-539637464). So, pipeline configurations should be updated after the new version of cypress comes out.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run e2e`

Launches the test runner for cypress end-to-end tests.

### `npm run ci`

Launches cypress tests in headless mode.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified, and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

### `npm run lint`

You can run ESLint with this command.

### `npm run format`

You can run Prettier with this command.
